#!/bin/bash

DIRECTORYTOLIST=/tmp

if [ $# -eq 1 -a -d "$1"]
then
    if [-d "$1"]; then
        DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found"
    fi 
fi

ls $DIRECTORYTOLIST