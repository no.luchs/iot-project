#!/bin/bash

HTMLOUTPUT="/opt/iotproject/index.html"
DIRECTORYTOLIST=/tmp

if [ $# -eq 1 -a -d "$1"]
then
    if [-d "$1"]; then
        DIRECTORYTOLIST=$1
    else
        echo "Directory $1 not found"
        exit 1
    fi 
fi





echo "<html><body>" > $HTMLOUTPUT
echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
date +%H:%M:%S >> $HTMLOUTPUT
ls /tmp >> $HTMLOUTPUT
cat $* >> $HTMLOUTPUT
echo "</body></html>" >> $HTMLOUTPUT